## 1.1.6

Apple health kit for flutter (Flutter Health) initial release

You can use it to get any of the following

Tested: 

* bodyFatPercentage
* height
* bodyMassIndex
* waistCircumference
* stepCount
* basalEnergyBurned
* activeEnergyBurned
* heartRate
* restingHeartRate
* walkingHeartRateAverage
* bodyTemperature
* bloodPressureSystolic
* bloodPressureDiastolic
* oxygenSaturation
* bloodGlucose
* electrodermalActivity 

Could not be tested:
  
* highHeartRateEvent
* lowHeartRateEvent
* irregularHeartRhythmEvent
